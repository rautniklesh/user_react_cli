import ReactDOM from 'react-dom';
/*import { Provider } from 'react-redux';*/
import React from 'react';
/*import { store, history} from './store';

import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';

import App from './components/App';*/
import './custom.css';
function BorderColor(props){
  return (
      <div className={'demo demo-'+props.color}>
        {props.children}
      </div>
    )
}

class App extends React.Component{
  constructor(props) {
    super(props);
  
    this.state = {};
  }
  render(){
    return (
        <div>
          <BorderColor color="red"><h1>Red Color</h1></BorderColor>
          <BorderColor color="green"><h1>Green Color</h1></BorderColor>
        </div>
      )
  }
}

ReactDOM.render(<App />,document.getElementById('root'))